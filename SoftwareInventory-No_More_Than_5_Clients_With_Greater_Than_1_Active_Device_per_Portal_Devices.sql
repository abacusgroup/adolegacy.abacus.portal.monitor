SELECT CASE WHEN COUNT(*) > 0 THEN 'Failed' ELSE 'Success' END AS 'Status' FROM (

SELECT DISTINCT t.Name, tsp.tenantId, sp.name as ServicePlan, mf.ActiveVersionId, sps.[SoftwareInvReportEntitlement?], spo.entitlementOptionId, mfv.ModifiedUtc
FROM [Billing_3_00].[dbo].[TenantServicePlans] AS tsp
INNER JOIN [TenantData_3_50].[dbo].[Tenants] t ON t.UniqueId = tsp.tenantId
INNER JOIN [Billing_3_00].[dbo].[ServicePlans] sp ON sp.id = tsp.servicePlanId
INNER JOIN (SELECT *, CASE WHEN entitlementId = 25 THEN 'TRUE' ELSE 'FALSE' END AS 'SoftwareInvReportEntitlement?' FROM [Billing_3_00].[dbo].[ServicePlanSettings]) sps ON sps.servicePlanId = sp.id
INNER JOIN [Billing_3_00].[dbo].[ServicePlanOverrides] spo ON spo.servicePlanSettingId = sps.id
INNER JOIN [Billing_3_00].[dbo].[Entitlements] e ON e.id = sps.entitlementId
INNER JOIN [Billing_3_00].[dbo].EntitlementOptions eo ON eo.entitlementId = e.id

LEFT JOIN 
(
SELECT DISTINCT TenantId, COUNT(Id) As numberOfWorkstations 
FROM [Devices_3_50].[dbo].[Workstation]
WHERE CAST(LastSeen AS DATE) = CAST(GETDATE() AS DATE)
GROUP BY TenantId
) w ON tsp.tenantId = w.TenantId

LEFT JOIN [FileManager].[dbo].[Distributions] fd ON tsp.tenantId = fd.TenantId
LEFT JOIN [FileManager].[dbo].[ManagedFiles] mf ON mf.Id = fd.ManagedFileId AND mf.Name = 'Software Inventory Report'
LEFT JOIN [FileManager].[dbo].[ManagedFileVersions] mfv ON mfv.Id = mf.ActiveVersionId

WHERE w.numberOfWorkstations > 1 
AND ((CAST(mfv.ModifiedUtc AS DATE) != CAST(GETDATE() AS DATE)) AND (mf.ActiveVersionId IS NOT NULL)) 
AND (((sps.[SoftwareInvReportEntitlement?] = 'TRUE') AND ((spo.entitlementOptionId = 65) OR (spo.entitlementOptionId IS NULL)))
OR ((sps.[SoftwareInvReportEntitlement?] = 'FALSE') AND (spo.entitlementOptionId = 65))
OR ((sps.[SoftwareInvReportEntitlement?] = 'FALSE') AND (mf.ActiveVersionId IS NOT NULL AND (spo.entitlementOptionId = 66 OR spo.entitlementOptionId IS NULL))))
AND tsp.isPending = 0
GROUP BY t.Name, tsp.tenantId, sp.name, w.numberOfWorkstations, mf.ActiveVersionId, sps.[SoftwareInvReportEntitlement?], spo.entitlementOptionId, mfv.ModifiedUtc
HAVING COUNT(*) > 5

) AS ID
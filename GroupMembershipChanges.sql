SELECT CASE WHEN COUNT(*) > 0 THEN 'Failed' ELSE 'Success' END AS 'Status' FROM (

 SELECT fhvf.id, RTRIM(fhvf.value) + ' ' + RTRIM(fhvl.value) AS 'GMC Full Name', right(gmc.[Member Name], len(gmc.[Member Name]) - charindex('\', gmc.[Member Name])) AS 'Form Full Name', ft.description FROM [FormsManager_3_00].[dbo].[FormHistoryValue] fhvf
 INNER JOIN [FormsManager_3_00].[dbo].[FormHistoryValue] fhvl ON fhvl.name = 'last-name'  AND fhvl.FormHistory_id = fhvf.id
 INNER JOIN [FormsManager_3_00].[dbo].[FormHistory] fh ON fh.id = fhvf.id
 INNER JOIN [FormsManager_3_00].[dbo].[FormTemplate] ft ON ft.id = fh.formTemplate_id
 RIGHT JOIN (SELECT * FROM [Audit_3_50].[dbo].[Group_Membership_Changes] WHERE [Change Type] = 'Added' AND [Member Name] IS NOT NULL) gmc ON (RTRIM(fhvf.value) + ' ' + RTRIM(fhvl.value)) = (right(gmc.[Member Name], len(gmc.[Member Name]) - charindex('\', gmc.[Member Name])))
 WHERE fhvf.name = 'first-name' 
 AND ft.description LIKE '%Add User%'
 AND (right(gmc.[Member Name], len(gmc.[Member Name]) - charindex('\', gmc.[Member Name]))) IS NOT NULL
 AND (RTRIM(fhvf.value) + ' ' + RTRIM(fhvl.value)) != (right(gmc.[Member Name], len(gmc.[Member Name]) - charindex('\', gmc.[Member Name])))

 ) AS ID
SELECT CASE WHEN COUNT(*) > 0 THEN 'Failed' ELSE 'Success' END AS 'Status' FROM (

SELECT DISTINCT t.Name, tsp.tenantId, sp.name as ServicePlan, mf.ActiveVersionId, sps.[FolderSizeReportEntitlement?], spo.entitlementOptionId, mfv.ModifiedUtc
FROM [Billing_3_00].[dbo].[TenantServicePlans] AS tsp
INNER JOIN [TenantData_3_50].[dbo].[Tenants] t ON t.UniqueId = tsp.tenantId
INNER JOIN [Billing_3_00].[dbo].[ServicePlans] sp ON sp.id = tsp.servicePlanId
INNER JOIN (SELECT *, CASE WHEN entitlementId = 21 THEN 'TRUE' ELSE 'FALSE' END AS 'FolderSizeReportEntitlement?' FROM [Billing_3_00].[dbo].[ServicePlanSettings]) sps ON sps.servicePlanId = sp.id
INNER JOIN [Billing_3_00].[dbo].[ServicePlanOverrides] spo ON spo.servicePlanSettingId = sps.id
INNER JOIN [Billing_3_00].[dbo].[Entitlements] e ON e.id = sps.entitlementId
INNER JOIN [Billing_3_00].[dbo].EntitlementOptions eo ON eo.entitlementId = e.id

LEFT JOIN [FileManager].[dbo].[Distributions] fd ON tsp.tenantId = fd.TenantId
LEFT JOIN [FileManager].[dbo].[ManagedFiles] mf ON mf.Id = fd.ManagedFileId AND mf.Name = 'Folder Size Report'
LEFT JOIN [FileManager].[dbo].[ManagedFileVersions] mfv ON mfv.Id = mf.ActiveVersionId

WHERE ((CAST(mfv.ModifiedUtc AS DATE) != CAST(GETDATE() AS DATE)) AND (mf.ActiveVersionId IS NOT NULL)) 
AND (((sps.[FolderSizeReportEntitlement?] = 'TRUE') AND ((spo.entitlementOptionId = 57) OR (spo.entitlementOptionId IS NULL)))
OR ((sps.[FolderSizeReportEntitlement?] = 'FALSE') AND (spo.entitlementOptionId = 57))
OR ((sps.[FolderSizeReportEntitlement?] = 'FALSE') AND (mf.ActiveVersionId IS NOT NULL AND (spo.entitlementOptionId = 58 OR spo.entitlementOptionId IS NULL))))
AND tsp.isPending = 0
GROUP BY t.Name, tsp.tenantId, sp.name, mf.ActiveVersionId, sps.[FolderSizeReportEntitlement?], spo.entitlementOptionId, mfv.ModifiedUtc

) AS ID
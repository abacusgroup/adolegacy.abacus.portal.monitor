DECLARE @workstationExpectedRecordCount int = 2;
DECLARE @limit int = 5;

SELECT CASE WHEN COUNT(final.numberOfWorkstationIds) > @limit THEN 'FAILED' ELSE 'SUCCESS' END AS Status, COUNT(final.Workstation_ParentId) AS NumberOfFailedRecords FROM (

SELECT t.Workstation_ParentId, t.Name, t.Description, COUNT(t.Workstation_ParentId) as numberOfWorkstationIds FROM (

SELECT * FROM [Devices_3_50].[dbo].[WorkstationProperty] start
WHERE Description = 'WarrantyStart' OR Name = 'WarrantyStart'

UNION ALL

SELECT * FROM [Devices_3_50].[dbo].[WorkstationProperty]
WHERE Description = 'WarrantyEnd' OR Name = 'WarrantyEnd'
) t
GROUP BY t.Workstation_ParentId, t.Name, t.Description
HAVING COUNT(t.Workstation_ParentId) < @workstationExpectedRecordCount

) final
GO
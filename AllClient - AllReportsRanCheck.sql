DECLARE @ExceptionLimit AS INTEGER = 0

SELECT DISTINCT t.Name, tsp.tenantId, sp.name as ServicePlan, mf.ActiveVersionId, mfv.ModifiedUtc, mf.Name
FROM [Billing_3_00].[dbo].[TenantServicePlans] AS tsp
INNER JOIN [TenantData_3_50].[dbo].[Tenants] t ON t.UniqueId = tsp.tenantId
INNER JOIN [Billing_3_00].[dbo].[ServicePlans] sp ON sp.id = tsp.servicePlanId
LEFT JOIN [FileManager].[dbo].[Distributions] fd ON tsp.tenantId = fd.TenantId
LEFT JOIN [FileManager].[dbo].[ManagedFiles] mf ON mf.Id = fd.ManagedFileId AND mf.Name IN ('All Client AntiVirus Activity Report','All Client Device Report','Windows Patch Status Report','Email Autentication Settings Report','All Client Entitlements Report','Client Security Settings Report', 'Third Party Patching QA Report', 'All Client Folder Size Report','All Client Software Report','All Clients - Device Inventory Alert Report','All Clients - KB4 ReCon Report','All Services','AtHoc Import File','Client Codes','Client Tags','Client User Locations','Duo Users Report','Inactive Clients with Services','O365 License Report','OneCallNow Import','Phishing Enrolled Daily Report','Phishing Entitled Daily Report','Roles and Controls','Server Details','Service Ticket Comparison Metrics','User License Report','User Matrix')
LEFT JOIN [FileManager].[dbo].[ManagedFileVersions] mfv ON mfv.Id = mf.ActiveVersionId

WHERE ((CAST(mfv.ModifiedUtc AS DATE) != CAST(GETDATE() AS DATE)) AND (mf.ActiveVersionId IS NOT NULL)) 
AND tsp.isPending = 0
GROUP BY t.Name, tsp.tenantId, sp.name, mf.ActiveVersionId, mfv.ModifiedUtc, mf.Name

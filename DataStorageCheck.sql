--Types:
--archive
--drive
--ezestorage
--ezetradarstorage
--mailbox
--tradarstorage
--vm

CREATE PROCEDURE dataStorageCheck @Days int, @type varchar(50)
AS

SELECT final.type, CAST(dates.Date AS Date) AS Date, final.dailyTotal, final.TypeAvg, final.TypeTotal, final.TypeVariation, final.TypeVariation, final.TypeStdEva, final.TypeTotalSampleSize, LAG(final.dailyTotal, 1) OVER (ORDER BY final.type, final.date) AS PreviousValue, CASE WHEN final.dailyTotal <= 0 OR final.dailyTotal IS NULL then 'Missing data!' WHEN final.dailyTotal < (final.TypeAvg - final.TypeStdEva * 3) THEN 'Below 3 Standard Deviations' WHEN final.dailyTotal / LAG(final.dailyTotal, 1) OVER (ORDER BY final.type, final.date) <= .8 THEN 'Daily Storage down +20%' WHEN final.dailyTotal / LAG(final.dailyTotal, 1) OVER (ORDER BY final.type, final.date) >= 1.2 THEN 'Daily Storage up +20%' ELSE '' END AS ALERT

FROM  (
SELECT  TOP (DATEDIFF(DAY, DATEADD(day, -@Days, getdate()), getdate()))
        Date = DATEADD(DAY, ROW_NUMBER() OVER(ORDER BY a.object_id), GETDATE() -@Days )
FROM    sys.all_objects a
        CROSS JOIN sys.all_objects b) AS dates

LEFT JOIN (

SELECT * FROM (

SELECT DISTINCT s.type, s.date, SUM(storageMbUsed) as dailyTotal, stats.totals as TypeTotal, stats.averages as TypeAvg, stats.variations as TypeVariation, SQRT(stats.variations) as TypeStdEva, stats.Sample as TypeTotalSampleSize FROM [Billing_3_00].[dbo].[ServiceStorage] s
INNER JOIN (
SELECT standarddeviation.type, SUM(standarddeviation.subtotals) AS totals, standarddeviation.averages, SUM(standarddeviation.variations) / (@Days -1) as variations, @Days AS Sample FROM (

SELECT DISTINCT s.type, s.date, SUM(s.storageMbUsed) as subtotals, average.averages, CASE WHEN SUM(s.storageMbUsed) - average.averages < 0 THEN 0 ELSE SQUARE(SUM(s.storageMbUsed) - average.averages) END AS variations FROM [Billing_3_00].[dbo].[ServiceStorage] s
INNER JOIN (

SELECT DISTINCT type, SUM(subtotals) / @Days as averages FROM (

SELECT DISTINCT type, SUM(storageMbUsed) as subtotals FROM [Billing_3_00].[dbo].[ServiceStorage]
WHERE date >= GETDATE() - @Days
GROUP BY type, date

) base
GROUP BY base.type
) average ON average.type = s.type

WHERE s.date >= GETDATE() - @Days
GROUP BY s.type, s.date, average.averages

) standarddeviation
GROUP BY standarddeviation.type, standarddeviation.averages
) stats ON stats.type = s.type

WHERE date >= GETDATE() - @Days
GROUP BY s.type, s.date, stats.totals, stats.averages, stats.variations, stats.Sample
) master

) final ON CAST(dates.Date AS Date) = CAST(final.date AS Date)
WHERE final.type = @type
GO
SELECT CASE WHEN COUNT(*) > 0 THEN 'Failed' ELSE 'Success' END AS 'Status' FROM (

SELECT DISTINCT t.Name, tsp.tenantId, sp.name as ServicePlan, mf.ActiveVersionId, mfv.ModifiedUtc
FROM [Billing_3_00].[dbo].[TenantServicePlans] AS tsp
INNER JOIN [TenantData_3_50].[dbo].[Tenants] t ON t.UniqueId = tsp.tenantId
INNER JOIN [Billing_3_00].[dbo].[ServicePlans] sp ON sp.id = tsp.servicePlanId


LEFT JOIN [FileManager].[dbo].[Distributions] fd ON tsp.tenantId = fd.TenantId
LEFT JOIN [FileManager].[dbo].[ManagedFiles] mf ON mf.Id = fd.ManagedFileId AND mf.Name = 'O365 License Report'
LEFT JOIN [FileManager].[dbo].[ManagedFileVersions] mfv ON mfv.Id = mf.ActiveVersionId

WHERE ((CAST(mfv.ModifiedUtc AS DATE) != CAST(GETDATE() AS DATE)) AND (mf.ActiveVersionId IS NOT NULL)) 
GROUP BY t.Name, tsp.tenantId, sp.name, mf.ActiveVersionId, mfv.ModifiedUtc

) AS ID
SELECT CASE WHEN COUNT(*) > 0 THEN 'Failed' ELSE 'Success' END AS 'Status' FROM (

SELECT DISTINCT t.Name, tsp.tenantId, sp.name as ServicePlan, mf.ActiveVersionId, sps.[FileServerPermissionsReportEntitlement?], spo.entitlementOptionId, mfv.ModifiedUtc
FROM [Billing_3_00].[dbo].[TenantServicePlans] AS tsp
INNER JOIN [TenantData_3_50].[dbo].[Tenants] t ON t.UniqueId = tsp.tenantId
INNER JOIN [Billing_3_00].[dbo].[ServicePlans] sp ON sp.id = tsp.servicePlanId
INNER JOIN (SELECT *, CASE WHEN entitlementId = 23 THEN 'TRUE' ELSE 'FALSE' END AS 'FileServerPermissionsReportEntitlement?' FROM [Billing_3_00].[dbo].[ServicePlanSettings]) sps ON sps.servicePlanId = sp.id
INNER JOIN [Billing_3_00].[dbo].[ServicePlanOverrides] spo ON spo.servicePlanSettingId = sps.id
INNER JOIN [Billing_3_00].[dbo].[Entitlements] e ON e.id = sps.entitlementId
INNER JOIN [Billing_3_00].[dbo].EntitlementOptions eo ON eo.entitlementId = e.id

LEFT JOIN [FileManager].[dbo].[Distributions] fd ON tsp.tenantId = fd.TenantId
LEFT JOIN [FileManager].[dbo].[ManagedFiles] mf ON mf.Id = fd.ManagedFileId AND mf.Name = 'File Server Permission Report'
LEFT JOIN [FileManager].[dbo].[ManagedFileVersions] mfv ON mfv.Id = mf.ActiveVersionId

WHERE ((CAST(mfv.ModifiedUtc AS DATE) != CAST(GETDATE() AS DATE)) AND (mf.ActiveVersionId IS NOT NULL)) 
AND (((sps.[FileServerPermissionsReportEntitlement?] = 'TRUE') AND ((spo.entitlementOptionId = 61) OR (spo.entitlementOptionId IS NULL)))
OR ((sps.[FileServerPermissionsReportEntitlement?] = 'FALSE') AND (spo.entitlementOptionId = 61))
OR ((sps.[FileServerPermissionsReportEntitlement?] = 'FALSE') AND (mf.ActiveVersionId IS NOT NULL AND (spo.entitlementOptionId = 62 OR spo.entitlementOptionId IS NULL))))
AND tsp.isPending = 0
GROUP BY t.Name, tsp.tenantId, sp.name, mf.ActiveVersionId, sps.[FileServerPermissionsReportEntitlement?], spo.entitlementOptionId, mfv.ModifiedUtc

) AS ID
SELECT CASE WHEN COUNT(*) > 0 THEN 'Failed' ELSE 'Success' END AS 'Status' FROM (

SELECT DISTINCT t.Name, tsp.tenantId, sp.name as ServicePlan, w.numberOfWorkStationDevices, nd.numberOfNetworkDevices, xm.numberOfMobileDevices, mf.ActiveVersionId, sps.[DeviceInvReportEntitlement?], spo.entitlementOptionId, mfv.ModifiedUtc
FROM [Billing_3_00].[dbo].[TenantServicePlans] AS tsp
INNER JOIN [TenantData_3_50].[dbo].[Tenants] t ON t.UniqueId = tsp.tenantId
INNER JOIN [Billing_3_00].[dbo].[ServicePlans] sp ON sp.id = tsp.servicePlanId
INNER JOIN (SELECT *, CASE WHEN entitlementId = 24 THEN 'TRUE' ELSE 'FALSE' END AS 'DeviceInvReportEntitlement?' FROM [Billing_3_00].[dbo].[ServicePlanSettings]) sps ON sps.servicePlanId = sp.id
INNER JOIN [Billing_3_00].[dbo].[ServicePlanOverrides] spo ON spo.servicePlanSettingId = sps.id
INNER JOIN [Billing_3_00].[dbo].[Entitlements] e ON e.id = sps.entitlementId
INNER JOIN [Billing_3_00].[dbo].EntitlementOptions eo ON eo.entitlementId = e.id

LEFT JOIN 
(
SELECT DISTINCT w.TenantId, COUNT(*) As numberOfWorkStationDevices
FROM [Devices_3_50].[dbo].[Workstation] w
WHERE CAST(w.LastSeen AS DATE) = CAST(GETDATE() AS DATE)
GROUP BY w.TenantId
) w ON tsp.tenantId = w.TenantId

LEFT JOIN 
(
SELECT DISTINCT nd.TenantId, COUNT(*) As numberOfNetworkDevices
FROM [Devices_3_50].[dbo].[NetworkDevice] nd
WHERE CAST(nd.LastSeen AS DATE) = CAST(GETDATE() AS DATE)
GROUP BY nd.TenantId
) nd ON tsp.tenantId = nd.TenantId

LEFT JOIN 
(
SELECT DISTINCT xm.TenantId, COUNT(*) As numberOfMobileDevices
FROM [Devices_3_50].[dbo].[XenMobile] xm
WHERE CAST(xm.LastSeen AS DATE) = CAST(GETDATE() AS DATE)
GROUP BY xm.TenantId
) xm ON tsp.tenantId = xm.TenantId

LEFT JOIN [FileManager].[dbo].[Distributions] fd ON tsp.tenantId = fd.TenantId
LEFT JOIN [FileManager].[dbo].[ManagedFiles] mf ON mf.Id = fd.ManagedFileId AND mf.name = 'Device Inventory Report'
LEFT JOIN [FileManager].[dbo].[ManagedFileVersions] mfv ON mfv.Id = mf.ActiveVersionId

WHERE (w.numberOfWorkStationDevices + nd.numberOfNetworkDevices + xm.numberOfMobileDevices) > 1 
AND ((CAST(mfv.ModifiedUtc AS DATE) != CAST(GETDATE() AS DATE)) AND (mf.ActiveVersionId IS NOT NULL))
AND (((sps.[DeviceInvReportEntitlement?] = 'TRUE') AND ((spo.entitlementOptionId = 63) OR (spo.entitlementOptionId IS NULL)))
OR ((sps.[DeviceInvReportEntitlement?] = 'FALSE') AND (spo.entitlementOptionId = 63))
OR ((sps.[DeviceInvReportEntitlement?] = 'FALSE') AND (mf.ActiveVersionId IS NOT NULL AND (spo.entitlementOptionId = 64 OR spo.entitlementOptionId IS NULL))))
AND tsp.isPending = 0
GROUP BY t.Name, tsp.tenantId, sp.name, w.numberOfWorkStationDevices, nd.numberOfNetworkDevices, xm.numberOfMobileDevices, mf.ActiveVersionId, sps.[DeviceInvReportEntitlement?], spo.entitlementOptionId, mfv.ModifiedUtc

) AS ID
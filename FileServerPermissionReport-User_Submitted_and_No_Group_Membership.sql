/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [Client]
      ,[Change Time]
      ,[Change Type]
      ,[Domain]
      ,[Group Name]
      ,[Member Name]
	  ,right([Member Name], len([Member Name]) - charindex('\', [Member Name])) AS 'User'
  FROM [Audit_3_50].[dbo].[Group_Membership_Changes]
  WHERE [Change Type] = 'Added'
